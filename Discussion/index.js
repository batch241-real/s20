// console.log("Hello World!");

// Repetition Control Structures

// While Loop
/*
	A while loop takes in an expression/condition
	If the condition evaluates to be true, the statements inside the code will be executed

	while(expression/condtion){
		statement
	}
*/

let count = 5;

while(count !== 0){

	// The current value of count is printed out
	console.log("While: "+ count);
	
	// Decreases the value of count by after every iteration.
	count --;
}

// Do While Loop
/*
	A do while loop workd a lot like while loop but it guarantees that the code will be executed atleast once.
	Syntax:
		do{
			statement
		} while(expression/condition)
*/

// let number = Number(prompt("Give me a number"));

// do {
// 	// the current value of number is printed out
// 	console.log("Do While: "+ number);
	
// 	// Increases the value of number by 1 after every iteration
// 	// number = number + 1
// 	number += 1;

// // Providing a number of 10 or greater will run the code block once but the loop will stop there
// } while(number < 10);


// For Loop
/*
	A for loop is more flexible than while and do-while loops.
	It consists of three parts
		1. The initial value which is the starting value.
		2. The expression/condition that will determine if the loop will run one more time.
		3. The final expression which determines how long loop will run (increment/decrement)
	Syntax:
		for(initial, expression/condition, finalExpression){
			statement
		}
*/
/*
	- this loop start from 0
	- this loop will run as long as the value of count is less than or equal to zero
	-this loop adds 1 after every run
*/

// for(let count = 0; count <= 20; count++){
// 	console.log(count);
// }

// Characters in string may be counted using the .length property
// Strings are special compared to other data types as it has access to functions and other places of information. Other primitive data doesn't have this.

// let myString = "Jeru"
// console.log(myString.length);

// // Accessing elements of a string
// // Individual characters of a string may be accessed using its index number
// // The first characters of a string has an index of '0'. The next is one and so on.
// console.log(myString[0]);
// console.log(myString[1]);
// console.log(myString[2]);
// console.log(myString[3]);

// // This loop prints the individual letters of the myString variable
// for (let x = 0; x < myString.length; x++){
// 	// The current value of myString is printed out using its index value
// 	console.log(myString[x]);
// }
/*
- this loop will print out the letters of the name individually but will print 3 if the letter is a vowel
- the loop starts with the value of zero assigned to 1
- this loop will run as long as the value of i is less than the length of the name
- this loop increments
*/

// let myName = "annejanette";

// for (let i = 0; i < myName.length; i++){

// 	// If the charter of your name is a vowel letter, instead of displaying the character, it will display the number 3
// 	// The "toLowerCase" function will chanfe the current letter being evaluated into small letters/lowercase
// 	if(
// 		myName[i].toLowerCase() == "a" ||
// 		myName[i].toLowerCase() == "e" ||
// 		myName[i].toLowerCase() == "i" ||
// 		myName[i].toLowerCase() == "o" ||
// 		myName[i].toLowerCase() == "u" ) {
// 		// print 3 if the condition is met
// 		console.log(3);
// 	} else {
// 		// print the non vowel characters
// 		console.log(myName[i]);
// 	}
// }

// Continue and Vreak
/*
	The "continue" statement allows the code to go to the next iteration of a loop without finishing the execution of a statements in a code block.
	The "break" statement is used to terminate the loop once a match has been found
*/

// for(let count = 0; count <= 20; count++){

// 	// If remainder is equal to 0
// 	if(count % 2 === 0){
// 		// tells the code to continue to the next iteration of the loop
// 		// ignores all the statements located after the continue statement
// 		continue;
// 	}

// 	// the current value of number is printed out if the remainder is not equal to 0
// 	console.log("Continue and Break: "+ count);

// 	if(count > 10){
// 		// tells the code to terminate/stop the loop even if the condition/expression of the loop defines that it should execute so long as the value of count is less than or equal to 20
// 		break;
// 	}
// }

let name = "jakedlexter"

for(let i = 0; i < name.length; i++){
	// the current letter is printed out based on its index
	console.log(name[i]);

	// if the value is a, continue to the next iteration of the loop
	if(name[i].toLowerCase() === "a"){
		console.log("Continue to the next iteration");
		continue;
	}

	// if the current letter is equal to d, stop the loop
	if(name[i] == "d"){
		break;
	}
}