// console.log("Hello!");

let number = Number(prompt("Please enter a number:"));

console.log("The number you provided is "+ number);

for(let i = number; i >= 0; i--){

	if(i <= 50){
		console.log("The current value is at 50. Terminating the loop.")
		break;
	}
	if(i % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}

	if(i % 5 === 0){
		console.log(i);
	}
}

let myString = "supercalifragilisticexpialidocious";

let consonants = "";

for(j = 0; j < myString.length; j++){

	if(
		myString[j] === "a" ||
		myString[j] === "e" ||
		myString[j] === "i" ||
		myString[j] === "o" ||
		myString[j] === "u"
		){
		continue;
	} else {
		consonants += myString[j];
	}
}

console.log(myString);
console.log(consonants);